import React from "react";
import { Link, useLocation } from "react-router-dom";

// Components
import Headline1 from "./headline1";
import Body1 from "./body1";

const NotFound: React.FC = () => {
  const { pathname } = useLocation();

  return (
    <>
      <Headline1>404 Page not found</Headline1>
      <Body1>
        Page <strong>{pathname}</strong> was not found. Please return to the&nbsp;
        <Link to="/">Home page</Link>.
      </Body1>
    </>
  );
};

export default NotFound;
