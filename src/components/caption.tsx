import React from "react";

// RMWC
import { Typography } from "@rmwc/typography";

// Styles
import "@rmwc/typography/styles";

const Caption: React.FC = ({ children }) => <Typography use="caption">{children}</Typography>;

export default Caption;
