import React from "react";

import ExternalLink from "./external-link";

import "./social.scss";

const Social: React.FC = () => (
  <aside className="follow-me">
    <ul>
      <li>
        <ExternalLink
          className="gitlab"
          href="https://gitlab.com/Sgiath"
          title="Follow me on GitLab">
          <span>Follow me on GitLab</span>
        </ExternalLink>
      </li>
      <li>
        <ExternalLink
          className="github"
          href="https://github.com/Sgiath"
          title="Follow me on Github">
          <span>Follow me on Github</span>
        </ExternalLink>
      </li>
      <li>
        <ExternalLink
          className="twitter"
          href="https://twitter.com/SgiathDev"
          title="Follow me on Twitter">
          <span>Follow me on Twitter</span>
        </ExternalLink>
      </li>
    </ul>
  </aside>
);

export default Social;
