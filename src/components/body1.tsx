import React from "react";

// RMWC
import { Typography } from "@rmwc/typography";

// Styles
import "@rmwc/typography/styles";

const Body1: React.FC = ({ children }) => (
  <Typography use="body1" tag="p">
    {children}
  </Typography>
);

export default Body1;
