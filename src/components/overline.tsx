import React from "react";

// RMWC
import { Typography } from "@rmwc/typography";

// Styles
import "@rmwc/typography/styles";

const Body1: React.FC = ({ children, ...props }) => (
  <Typography use="overline" {...props}>
    {children}
  </Typography>
);

export default Body1;
