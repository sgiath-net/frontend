import React from "react";

// RMWC
import { Typography } from "@rmwc/typography";

// Styles
import "@rmwc/typography/styles";

const Headline1: React.FC = ({ children }) => (
  <Typography use="headline3" tag="h1">
    {children}
  </Typography>
);

export default Headline1;
