import React from "react";

// Local
import firebase from "../firebase";

type Props = Omit<
  React.AnchorHTMLAttributes<HTMLAnchorElement>,
  "target" | "rel" | "onClick" | "onKeyPress" | "tabIndex"
>;

const ExternalLink: React.FC<Props> = ({ children, href, ...props }) => {
  const handleClick = React.useCallback(
    () =>
      firebase.logEvent("external_link_click", {
        eventAction: "click",
        eventCategory: "Outbound Link",
        eventLabel: href,
      }),
    [href],
  );

  return (
    <a
      target="_blank"
      rel="noopener noreferrer"
      onClick={handleClick}
      onKeyPress={handleClick}
      tabIndex={0}
      href={href}
      {...props}>
      {children}
    </a>
  );
};

export default ExternalLink;
