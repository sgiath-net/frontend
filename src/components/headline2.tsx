import React from "react";

// RMWC
import { Typography } from "@rmwc/typography";

// Styles
import "@rmwc/typography/styles";

const Headline2: React.FC = ({ children }) => (
  <Typography use="headline4" tag="h2">
    {children}
  </Typography>
);

export default Headline2;
