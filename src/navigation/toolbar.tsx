import React from "react";

// RMWC
import {
  TopAppBar,
  TopAppBarRow,
  TopAppBarTitle,
  TopAppBarSection,
  TopAppBarNavigationIcon,
} from "@rmwc/top-app-bar";

// Local
import { useToggleDrawer } from "./actions";
import { ThemeSwitch } from "../theme";
import { LoginButton, LogoutButton } from "../authentication";
import { useIsLogged } from "../authentication/hooks";

// Styles
import "@rmwc/top-app-bar/styles";

const MyToolbar: React.FC = () => {
  const isLogged = useIsLogged();
  const toggleDrawer = useToggleDrawer();

  return (
    <TopAppBar fixed>
      <TopAppBarRow>
        <TopAppBarSection alignStart>
          <TopAppBarNavigationIcon onClick={toggleDrawer} icon="menu" />
          <TopAppBarTitle>Sgiath.dev</TopAppBarTitle>
        </TopAppBarSection>
        <TopAppBarSection alignEnd role="toolbar">
          <ThemeSwitch />
          {isLogged ? <LogoutButton /> : <LoginButton />}
        </TopAppBarSection>
      </TopAppBarRow>
    </TopAppBar>
  );
};

export default MyToolbar;
