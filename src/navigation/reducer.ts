import actions from "./actions";
import { initialState } from "./state";

// Types
import type { NavigationState } from "./state";
import type { Reducer } from "redux";

const reducer: Reducer<NavigationState> = (state = initialState, { type }) => {
  switch (type) {
    case actions.openDrawer.TYPE:
      return { ...state, isOpen: true };

    case actions.closeDrawer.TYPE:
      return { ...state, isOpen: false };

    case actions.toggleDrawer.TYPE:
      return { ...state, isOpen: !state.isOpen };

    default:
      return state;
  }
};

export default reducer;
