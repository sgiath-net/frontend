import React from "react";
import { Link, useLocation } from "react-router-dom";

// RMWC
import { Drawer, DrawerContent } from "@rmwc/drawer";
import { List, ListItem, ListItemGraphic, ListItemText } from "@rmwc/list";

// Local
import { useCloseDrawer } from "./actions";
import { useIsOpen } from "./state";

// Styles
import "@rmwc/theme/styles";
import "@rmwc/drawer/styles";
import "@rmwc/icon/styles";
import "@rmwc/list/styles";
import "./drawer.scss";

const links = [
  { path: "/", icon: "home", name: "Home" },
  { path: "/resume", icon: "folder_shared", name: "Resume" },
  { path: "/projects", icon: "code", name: "Projects" },
  { path: "/talks", icon: "mic", name: "Talks" },
  { path: "/blogs", icon: "description", name: "Blogs" },
  { path: "/contact", icon: "alternate_email", name: "Contact" },
];

const TemporaryDrawer: React.FC = () => {
  const closeDrawer = useCloseDrawer();
  const isOpen = useIsOpen();
  const { pathname } = useLocation();

  return (
    <Drawer dismissible theme="surface" className="drawer" open={isOpen} onClose={closeDrawer}>
      <DrawerContent>
        <List>
          {links.map(({ path, icon, name }) => (
            <ListItem key={path} activated={pathname === path} tag={Link} to={path}>
              <ListItemGraphic icon={icon} theme={["textIconOnBackground"]} />
              <ListItemText>{name}</ListItemText>
            </ListItem>
          ))}
        </List>
      </DrawerContent>
    </Drawer>
  );
};

export default TemporaryDrawer;
