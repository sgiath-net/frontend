import { useSelector } from "react-redux";

// Types
import type { RootState } from "../store";

export interface NavigationState {
  readonly isOpen: boolean;
}

// Initial state is based on the mobile vs desktop
export const initialState: NavigationState = {
  isOpen: !window.matchMedia("(max-width: 720px)").matches,
};

export const useIsOpen = () => useSelector((state: RootState) => state.drawer.isOpen);
