import React from "react";
import { makeActionCreator, Action } from "redux-toolbelt";
import { useDispatch } from "react-redux";

interface NavigationAction extends Action {
  type: "@drawer/OPEN" | "@drawer/CLOSE" | "@drawer/TOGGLE";
}

const drawerActionCreator = makeActionCreator.withDefaults<NavigationAction>({
  prefix: "@drawer/",
});

export const openDrawer = drawerActionCreator("OPEN");
export const closeDrawer = drawerActionCreator("CLOSE");
export const toggleDrawer = drawerActionCreator("TOGGLE");

export const useCloseDrawer = () => {
  const dispatch = useDispatch();
  return React.useCallback(() => {
    dispatch(closeDrawer());
  }, [dispatch]);
};

export const useToggleDrawer = () => {
  const dispatch = useDispatch();
  return React.useCallback(() => {
    dispatch(toggleDrawer());
  }, [dispatch]);
};

export default {
  openDrawer,
  closeDrawer,
  useCloseDrawer,
  toggleDrawer,
  useToggleDrawer,
};
