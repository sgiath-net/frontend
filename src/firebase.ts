import firebase from "firebase/app";
import "firebase/analytics";
import "firebase/auth";
import "firebase/firestore";
import "firebase/performance";

// When updating don't forget to also update config at /public/firebase-messaging-sw.js
const firebaseConfig = {
  apiKey: "AIzaSyDuKfa-kFfcvMeohvjKLB58Qy16BlQnAcs",
  authDomain: "sgiath-net.firebaseapp.com",
  databaseURL: "https://sgiath-net.firebaseio.com",
  projectId: "sgiath-net",
  storageBucket: "sgiath-net.appspot.com",
  messagingSenderId: "987905182371",
  appId: "1:987905182371:web:d4fd9ba6f10a213ed10d16",
  measurementId: "G-PJPJ21SHLZ",
};

const init = (): void => {
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
  firebase.firestore();
  firebase.performance();
};

const logEvent = (eventName: string, eventParams: object): void =>
  firebase.analytics().logEvent(eventName, eventParams);

const login = (userId: string): void => firebase.analytics().setUserId(userId);

const logout = (): void => login("");

export default {
  init,
  logEvent,
  login,
  logout,
};
