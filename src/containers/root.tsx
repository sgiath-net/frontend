import React from "react";

import { Provider } from "react-redux";
import { ReactReduxFirebaseProvider } from "react-redux-firebase";
import { createFirestoreInstance } from "redux-firestore";
import { ConnectedRouter } from "connected-react-router";

// RMWC
import { RMWCProvider } from "@rmwc/provider";

// Firebase
import firebase from "firebase/app";

// Local
import App from "./app";
import { rrfConfig } from "../config";

// Types
import type { History } from "history";
import type { SgiathStore } from "../store";

// Styles
import "@rmwc/theme/styles";

interface Props {
  store: SgiathStore;
  history: History;
}

const Root: React.FC<Props> = ({ store, history }) => (
  <Provider store={store}>
    <ReactReduxFirebaseProvider
      firebase={firebase}
      config={rrfConfig}
      dispatch={store.dispatch}
      createFirestoreInstance={createFirestoreInstance}>
      <ConnectedRouter history={history}>
        <React.StrictMode>
          <RMWCProvider>
            <App />
          </RMWCProvider>
        </React.StrictMode>
      </ConnectedRouter>
    </ReactReduxFirebaseProvider>
  </Provider>
);

export default Root;
