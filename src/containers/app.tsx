import React from "react";
import { Route, Switch } from "react-router-dom";

// RMWC
import { DrawerAppContent } from "@rmwc/drawer";
import { TopAppBarFixedAdjust } from "@rmwc/top-app-bar";
import { Theme } from "@rmwc/theme";

// Pages
import Home from "../pages/home";
import Resume from "../pages/resume";
import Projects from "../pages/projects";
import Talks from "../pages/talks";
import Blogs from "../pages/blogs";
import Contact from "../pages/contact";

// Components
import NotFound from "../components/not-found";
import Social from "../components/social";
import { Drawer, Toolbar } from "../navigation";

// Styles
import "./app.scss";

const App: React.FC = () => (
  <>
    <Toolbar />
    <TopAppBarFixedAdjust>
      <Drawer />
      <DrawerAppContent tag="article" className="drawer-app-content">
        <Theme use="textPrimaryOnBackground">
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/resume">
              <Resume />
            </Route>
            <Route path="/projects">
              <Projects />
            </Route>
            <Route path="/talks">
              <Talks />
            </Route>
            <Route path="/blogs">
              <Blogs />
            </Route>
            <Route path="/contact">
              <Contact />
            </Route>
            <Route>
              <NotFound />
            </Route>
          </Switch>
        </Theme>
      </DrawerAppContent>
    </TopAppBarFixedAdjust>
    <Social />
  </>
);

export default App;
