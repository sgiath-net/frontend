import React from "react";
import { Route, Switch } from "react-router-dom";

// Components
import Body1 from "../../components/body1";
import Caption from "../../components/caption";
import ExternalLink from "../../components/external-link";
import Headline1 from "../../components/headline1";
import Headline2 from "../../components/headline2";

const ProjectsHome: React.FC = () => (
  <>
    <Headline1>Projects</Headline1>

    {/* BlueLabs ID */}
    <Headline2>BlueLabs ID</Headline2>
    <Caption>Interview project for company BlueLabs.</Caption>
    <Body1>I did this project as my interview task for company Blue Labs.</Body1>
    <Body1>
      <ExternalLink href="https://gitlab.com/Sgiath/bluelabs-id" title="GitLab">
        Source code
      </ExternalLink>
    </Body1>

    {/* MamonCity */}
    <Headline2>MamonCity</Headline2>
    <Caption>Application for live game</Caption>
    <Body1>
      I created this application during first half of 2019 and we played the game at the end of
      the August of 2019. The game was supposed to simulate "real" economics of the city where
      players objective was to accumulate as much money as possible. The city featured different
      kind of businesses, prison, bank and also stock market which automatically calculated how
      much was business worth based on the business turnover.
    </Body1>
    <Body1>
      I was forced to do a lot of changes last-minute because rules of the live game were changing
      pretty drastically based on our abilitiy to realize those rules in real life. So the code
      can be mess sometimes. I hope to get back to it and fix it one day.
    </Body1>
    <Body1>
      I used it as opportunity to try{" "}
      <ExternalLink
        href="https://github.com/phoenixframework/phoenix_live_view"
        title="Official Repo">
        Phoenix LiveView
      </ExternalLink>{" "}
      and I have to say that it was great experience. I definitelly like it.
    </Body1>
    <Body1>
      <ExternalLink href="https://gitlab.com/Sgiath/sinai" title="GitLab">
        Source code
      </ExternalLink>
    </Body1>
  </>
);

const Projects: React.FC = () => (
  <Switch>
    <Route exact path="/projects" component={ProjectsHome} />
  </Switch>
);

export default Projects;
