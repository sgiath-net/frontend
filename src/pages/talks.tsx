import React from "react";
import { Route, Switch } from "react-router-dom";

// Components
import Body1 from "../components/body1";
import Caption from "../components/caption";
import ExternalLink from "../components/external-link";
import Headline1 from "../components/headline1";
import Headline2 from "../components/headline2";

const TalksHome: React.FC = () => (
  <>
    <Headline1>Talks</Headline1>
    At the end of 2018 I started doing talks on conferences and meetups and I found that I like
    it. Here are some examples of my talks.
    <Headline2>Moving Fast! - Boulder</Headline2>
    <Caption>
      9<sup>th</sup> May 2019
    </Caption>
    <Body1>
      I presented demo on the{" "}
      <ExternalLink href="https://www.meetup.com/Moving-Fast-Boulder/events/259968018/">
        Moving Fast! - Boulder
      </ExternalLink>{" "}
      in Colorado about Kubernetes and deploying application to Cloud environment. I presented
      together with VP Engineering GitHub, Sha Ma and CEO of BubbleIQ Fletcher Richman.
      <br />
      <ExternalLink href="https://gitlab.com/profiq/moving-fast">Source Code</ExternalLink>
    </Body1>
    <Headline2>Moving Fast! #2 - Progressive Languages</Headline2>
    <Caption>
      11<sup>th</sup> February 2019
    </Caption>
    <Body1>
      I presented on second{" "}
      <ExternalLink href="https://www.meetup.com/MOVING-FAST/events/257831513/">
        Moving Fast
      </ExternalLink>{" "}
      meetup about <ExternalLink href="https://elixir-lang.org/">Elixir</ExternalLink> - new
      functional language based on Erlang VM.
    </Body1>
    <Headline2>Pyvo Ostrava</Headline2>
    <Caption>
      9<sup>th</sup> January 2019
    </Caption>
    <Body1>
      I presented the same demo on the Ostrava Pyvo. But it was focused more on the general
      capabilities of Cloud.
      <br />
      <ExternalLink href="https://gitlab.com/profiq/moving-fast">Source Code</ExternalLink>
    </Body1>
    <Headline2>Moving Fast! #1 - How to move fast</Headline2>
    <Caption>
      8<sup>th</sup> November 2018
    </Caption>
    <Body1>
      I presented demo on the{" "}
      <ExternalLink href="https://www.meetup.com/MOVING-FAST/events/255725390/">
        Moving Fast!
      </ExternalLink>{" "}
      event about deploying Django application to Kubernetes cluster in 30 min. The code is also
      interesting since it uses asynchronous Django. The application is simple chat which uses
      GraphQL API and WebSockets for realtime communication.
      <br />
      <ExternalLink href="https://gitlab.com/profiq/moving-fast">Source Code</ExternalLink>
    </Body1>
  </>
);

const Talks: React.FC = () => (
  <Switch>
    <Route exact path="/talks" component={TalksHome} />
  </Switch>
);

export default Talks;
