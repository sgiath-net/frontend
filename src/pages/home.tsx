import React from "react";
import { Link } from "react-router-dom";

// Components
import Body1 from "../components/body1";
import ExternalLink from "../components/external-link";
import Headline1 from "../components/headline1";

const Home: React.FC = () => (
  <>
    <Headline1>Home</Headline1>
    <Body1>
      Welcome on my personal web page where I present all of my small projects for everyone to
      use, test and get inspired by them. You can find more about them at{" "}
      <Link to="/projects">Projects page</Link>.
    </Body1>
    <Body1>
      I currently work as Fractional CTO for startups including{" "}
      <ExternalLink href="https://planesty.com/">Planesty</ExternalLink> and{" "}
      <ExternalLink href="https://serenityapp.com/">Serentiy</ExternalLink>.
    </Body1>
    <Body1>
      Areas of my interest are:{" "}
      <ExternalLink href="https://elixir-lang.org/">Elixir</ExternalLink>,{" "}
      <ExternalLink href="https://www.python.org/">Python</ExternalLink>, functional programming,{" "}
      <ExternalLink href="https://martinfowler.com/eaaDev/EventSourcing.html">
        EventSourcing
      </ExternalLink>
      , <ExternalLink href="https://martinfowler.com/bliki/CQRS.html">CQRS</ExternalLink> and DDD.
    </Body1>
    <Body1>
      I am primarily backend developer and architect but I can also develop some frontend
      application in React (this page is my best effort to create some nice graphic UI though).
    </Body1>
  </>
);

export default Home;
