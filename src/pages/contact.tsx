import React from "react";

// Components
import Body1 from "../components/body1";
import ExternalLink from "../components/external-link";
import Headline1 from "../components/headline1";

const Contacts: React.FC = () => (
  <>
    <Headline1>Contact</Headline1>
    <Body1>
      You can find me on{" "}
      <ExternalLink href="https://stackoverflow.com/users/7411901/sgiath" title="StackOverflow">
        StackOverflow
      </ExternalLink>{" "}
      or write me directly on email{" "}
      <ExternalLink href="mailto:FilipVavera@sgiath.dev">FilipVavera@sgiath.dev</ExternalLink>
    </Body1>
    <Body1>
      My GPG key:{" "}
      <ExternalLink href="http://hkps.pool.sks-keyservers.net/pks/lookup?op=vindex&fingerprint=on&search=0x70F9C7DE34CB3BC8">
        0x70F9C7DE34CB3BC8
      </ExternalLink>
    </Body1>
    <Body1>
      If you want, for some reason, send me Bitcoins you can use this address:{" "}
      <ExternalLink
        href="bitcoin:bc1qj2du7dypmu05jre2te2unwk3jawasgxesvrza7?label=Sgiath.dev"
        title="Bitcoin">
        bc1qj2du7dypmu05jre2te2unwk3jawasgxesvrza7
      </ExternalLink>
    </Body1>
  </>
);

export default Contacts;
