import React from "react";

// Components
import Body1 from "../components/body1";
import Headline1 from "../components/headline1";

const Blogs: React.FC = () => (
  <>
    <Headline1>Blogs</Headline1>
    <Body1>Here will be my blogs</Body1>
  </>
);

export default Blogs;
