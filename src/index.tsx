import React from "react";
import ReactDOM from "react-dom";

import { createBrowserHistory } from "history";

import * as serviceWorker from "./service-worker";
import Root from "./containers/root";
import firebase from "./firebase";
import configureMessaging from "./configureMessaging";
import { configureStore } from "./store";

// Main CSS
import "@rmwc/theme/styles";
import "./style.scss";

// Initialize Firebase
firebase.init();

// Configure history
const history = createBrowserHistory();

// Configure Store
const store = configureStore(history);

// Render React
const renderApp = () =>
  ReactDOM.render(<Root store={store} history={history} />, document.getElementById("root"));

// Hot reload App
if (process.env.NODE_ENV !== "production" && module.hot) {
  module.hot.accept("./containers/root", renderApp);
}

renderApp();

// Register Service Worker
serviceWorker.register({
  onUpdate: () => {},
  onSuccess: () => {},
});

// Configure Notifications
configureMessaging();
