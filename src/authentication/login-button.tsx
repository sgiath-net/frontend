import React from "react";
import { useFirebase } from "react-redux-firebase";

// RMWC
import { Button } from "@rmwc/button";

// Types
import type { Credentials } from "react-redux-firebase";

// Styles
import "@rmwc/button/styles";

const LoginButton: React.FC = () => {
  const firebase = useFirebase();

  const loginOpts: Credentials = {
    provider: "google",
    type: "popup",
  };

  const handleClick = React.useCallback(() => firebase.login(loginOpts), [firebase, loginOpts]);

  return <Button onClick={handleClick} theme="onPrimary" label="Login" />;
};

export default LoginButton;
