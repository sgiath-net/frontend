import React from "react";
import { useFirebase } from "react-redux-firebase";

// RMWC
import { Avatar } from "@rmwc/avatar";
import { Button } from "@rmwc/button";

// Local
import { useAvatarUrl, useProfileName } from "./hooks";

// Styles
import "@rmwc/avatar/styles";
import "@rmwc/button/styles";

const LoginButton: React.FC = () => {
  const firebase = useFirebase();
  const avatarUrl = useAvatarUrl();
  const displayName = useProfileName();

  return (
    <Button
      onClick={firebase.logout}
      label="Logout"
      theme="onPrimary"
      icon={<Avatar src={avatarUrl} name={displayName} theme={["onPrimary"]} />}
    />
  );
};

export default LoginButton;
