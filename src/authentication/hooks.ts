import { useSelector } from "react-redux";

// Types
import type { RootState } from "../store";

export const useIsLogged = () => {
  const isLoaded = useSelector((state: RootState) => state.firebase.auth.isLoaded);
  const isEmpty = useSelector((state: RootState) => state.firebase.auth.isEmpty);

  return isLoaded && !isEmpty;
};

export const useAvatarUrl = () =>
  useSelector((state: RootState) => state.firebase.profile.avatarUrl);

export const useProfileName = () =>
  useSelector((state: RootState) => state.firebase.profile.displayName);
