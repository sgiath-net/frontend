// Saga
import { all, take } from "redux-saga/effects";

// Actions
import firebase from "../firebase";
import { actionTypes } from "react-redux-firebase";

// Watch for login action
function* login() {
  while (true) {
    const {
      auth: { uid },
    } = yield take(actionTypes.LOGIN);

    firebase.login(uid);
    firebase.logEvent("login", { method: "Google" });
  }
}

// Watch for lougout action
function* logout() {
  while (true) {
    yield take(actionTypes.LOGOUT);

    firebase.logout();
    firebase.logEvent("logout", { event_category: "Auth", event_label: "logout" });
  }
}

export default function* analyticsSaga() {
  yield all([login(), logout()]);
}
