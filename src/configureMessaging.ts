import firebase, { messaging } from "firebase/app";
import "firebase/messaging";

const handleNotificationsToken = async (msg: messaging.Messaging) => {
  try {
    const currentToken = await msg.getToken({
      vapidKey:
        "BLfANLEUPiPYpIuJzyloROMKLEVL6fTu_ZXdjOI2JDJwP9aYp0kgCi3iLJp54NvSKTuwFJh8NiprJOnVNKZu0cI",
    });
    console.log(currentToken);
    // TODO (Filip Vavera): do something with the token
    // https://firebase.google.com/docs/cloud-messaging/js/client#retrieve-the-current-registration-token
  } catch (error) {
    console.error("An error occurred while obtaining the Token.", error);
  }
};

export const requestMessagingPermissions = async (msg: messaging.Messaging) => {
  if (!("Notification" in window)) {
    console.log("Browser doesn't support notifications");
  } else if (Notification.permission === "granted") {
    console.log("Notification permission already granted!");
    handleNotificationsToken(msg);
  } else if (Notification.permission === "denied") {
    console.log("Notification permission already denied!");
  } else {
    const permission = await Notification.requestPermission();

    if (permission === "granted") {
      console.log("Notification permission granted.");
      handleNotificationsToken(msg);
    } else if (permission === "denied") {
      console.log("Permission denied!");
    }
  }
};

const configureMessaging = () => {
  const messaging = firebase.messaging();

  // Handle incoming messages. Called when:
  // - a message is received while the app has focus
  // - the user clicks on an app notification created by a service worker
  //   `messaging.setBackgroundMessageHandler` handler.
  messaging.onMessage((payload) => {
    console.log("Message received. ", payload);
  });

  // requestMessagingPermissions(firebase.messaging());

  return messaging;
};

export default configureMessaging;
