import { useSelector } from "react-redux";

// Local
import { getTheme } from "./utils";

// Types
import type { RootState } from "../store";

export type Theme = "dark" | "light";

// Load initial theme from local storage
export const initialState: Theme = getTheme();

export const useTheme = (): Theme => useSelector((state: RootState) => state.theme);
