// Local
import actions from "./actions";
import { initialState } from "./state";

// Types
import type { Reducer } from "redux";
import type { Theme } from "./state";

const reducer: Reducer<Theme> = (state = initialState, action) => {
  switch (action.type) {
    case actions.setTheme.TYPE:
      return action.payload;

    default:
      return state;
  }
};

export default reducer;
