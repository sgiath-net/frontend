import { takeEvery, select } from "redux-saga/effects";

// Local
import { setDarkTheme, setLightTheme, saveTheme } from "./utils";
import { setTheme as action } from "./actions";

// Types
import type { Theme } from "./state";
import type { ThemeAction } from "./actions";
import type { RootState } from "../store";

/**
 * Helper function to set theme and save it to the localStorage
 *
 * @param action dispatched ThemeAction
 */
const setTheme = ({ payload }: ThemeAction): void => {
  switch (payload) {
    case "dark":
      setDarkTheme();
      break;

    case "light":
      setLightTheme();
      break;

    default:
      break;
  }

  saveTheme(payload as Theme);
};

export default function* themeSaga() {
  // At the start check current mode from store
  const mode: Theme = yield select<(state: RootState) => Theme>(({ theme }) => theme);

  // Set theme based on that
  if (mode === "dark") {
    setDarkTheme();
  } else {
    setLightTheme();
  }

  // Take every action and save the settings in localStore
  yield takeEvery([action.TYPE], setTheme);
}
