import { makeActionCreator } from "redux-toolbelt";
import { useDispatch } from "react-redux";

// Types
import type { GenericAction } from "redux-toolbelt";
import type { Theme } from "./state";

export type ThemeAction = GenericAction<Theme, Record<string, unknown>>;

export const setTheme = makeActionCreator<ThemeAction>("@theme/SET_THEME");

export const useSetTheme = () => {
  const dispatch = useDispatch();

  return (theme: Theme): void => {
    dispatch(setTheme(theme));
  };
};

export default {
  setTheme,
  useSetTheme,
};
