export default {
  dark: ["#282828", "#3c3836", "#504945", "#665c54", "#7c6f64"],
  light: ["#fbf1c7", "#ebdbb2", "#d5c4a1", "#bdae93", "#a89984"],

  red: {
    bright: "#fb4934",
    neutral: "#cc241d",
    faded: "#9d0006",
  },

  green: {
    bright: "#b8bb26",
    neutral: "#98971a",
    faded: "#79740e",
  },

  yellow: {
    bright: "#fabd2f",
    neutral: "#d79921",
    faded: "#b57614",
  },

  blue: {
    bright: "#83a598",
    neutral: "#458588",
    faded: "#076678",
  },

  purple: {
    bright: "#d3869b",
    neutral: "#b16286",
    faded: "#8f3f71",
  },

  aqua: {
    bright: "#8ec07c",
    neutral: "#689d6a",
    faded: "#427b58",
  },

  orange: {
    bright: "#fe8019",
    neutral: "#d65d0e",
    faded: "#af3a03",
  },
};
