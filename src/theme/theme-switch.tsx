import React from "react";

// RMWC
import { Switch } from "@rmwc/switch";
import { Typography } from "@rmwc/typography";

// Local
import { useSetTheme } from "./actions";
import { useTheme } from "./state";

// Styles
import "@rmwc/switch/styles";
import "@rmwc/typography/styles";

interface HandleChange {
  (event: React.FormEvent<HTMLInputElement>): void;
}

const ThemeSwitch: React.FC = () => {
  const theme = useTheme();
  const setTheme = useSetTheme();

  const handleChange: HandleChange = React.useCallback(
    ({ currentTarget: { checked } }) => setTheme(checked ? "dark" : "light"),
    [setTheme],
  );

  return (
    <Switch checked={theme === "dark"} onChange={handleChange}>
      <Typography use="button" theme="onPrimary" style={{ paddingLeft: "7px" }}>
        Dark Mode
      </Typography>
    </Switch>
  );
};

export default ThemeSwitch;
