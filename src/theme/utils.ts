import colors from "./gruvbox";

// Types
import type { Theme } from "./state";

export const setDarkTheme = (): void => {
  const root = document.documentElement;

  root.style.setProperty("--mdc-theme-primary", colors.orange.bright);
  root.style.setProperty("--mdc-theme-secondary", colors.blue.faded);
  root.style.setProperty("--mdc-theme-error", colors.red.bright);
  root.style.setProperty("--mdc-theme-background", colors.dark[0]);
  root.style.setProperty("--mdc-theme-surface", colors.dark[2]);
  root.style.setProperty("--mdc-theme-on-primary", colors.dark[1]);
  root.style.setProperty("--mdc-theme-on-secondary", colors.dark[1]);
  root.style.setProperty("--mdc-theme-on-error", colors.dark[1]);
  root.style.setProperty("--mdc-theme-on-surface", colors.light[0]);
  root.style.setProperty("--mdc-theme-text-primary-on-background", colors.light[1]);
  root.style.setProperty("--mdc-theme-text-primary-on-dark", colors.light[1]);
  root.style.setProperty("--mdc-theme-text-primary-on-light", colors.dark[1]);
  root.style.setProperty("--mdc-theme-text-secondary-on-background", colors.light[1]);
  root.style.setProperty("--mdc-theme-text-secondary-on-dark", colors.light[1]);
  root.style.setProperty("--mdc-theme-text-secondary-on-light", colors.dark[1]);
  root.style.setProperty("--mdc-theme-text-icon-on-background", colors.light[1]);
  root.style.setProperty("--mdc-theme-text-icon-on-dark", colors.light[1]);
  root.style.setProperty("--mdc-theme-text-icon-on-light", colors.dark[1]);
  root.style.setProperty("--mdc-theme-text-hint-on-background", colors.light[1]);
  root.style.setProperty("--mdc-theme-text-hint-on-dark", colors.light[1]);
  root.style.setProperty("--mdc-theme-text-hint-on-light", colors.dark[1]);
  root.style.setProperty("--mdc-theme-text-disabled-on-background", colors.light[1]);
  root.style.setProperty("--mdc-theme-text-disabled-on-dark", colors.light[1]);
  root.style.setProperty("--mdc-theme-text-disabled-on-light", colors.dark[1]);
};

export const setLightTheme = (): void => {
  const root = document.documentElement;

  root.style.setProperty("--mdc-theme-primary", colors.orange.bright);
  root.style.setProperty("--mdc-theme-secondary", colors.blue.neutral);
  root.style.setProperty("--mdc-theme-error", colors.red.bright);
  root.style.setProperty("--mdc-theme-background", colors.light[0]);
  root.style.setProperty("--mdc-theme-surface", colors.light[1]);
  root.style.setProperty("--mdc-theme-on-primary", colors.dark[1]);
  root.style.setProperty("--mdc-theme-on-secondary", colors.dark[1]);
  root.style.setProperty("--mdc-theme-on-error", colors.dark[1]);
  root.style.setProperty("--mdc-theme-on-surface", colors.dark[0]);
  root.style.setProperty("--mdc-theme-text-primary-on-background", colors.dark[1]);
  root.style.setProperty("--mdc-theme-text-primary-on-dark", colors.light[1]);
  root.style.setProperty("--mdc-theme-text-primary-on-light", colors.dark[1]);
  root.style.setProperty("--mdc-theme-text-secondary-on-background", colors.dark[1]);
  root.style.setProperty("--mdc-theme-text-secondary-on-dark", colors.light[1]);
  root.style.setProperty("--mdc-theme-text-secondary-on-light", colors.dark[1]);
  root.style.setProperty("--mdc-theme-text-icon-on-background", colors.dark[1]);
  root.style.setProperty("--mdc-theme-text-icon-on-dark", colors.light[1]);
  root.style.setProperty("--mdc-theme-text-icon-on-light", colors.dark[1]);
  root.style.setProperty("--mdc-theme-text-hint-on-background", colors.dark[1]);
  root.style.setProperty("--mdc-theme-text-hint-on-dark", colors.light[1]);
  root.style.setProperty("--mdc-theme-text-hint-on-light", colors.dark[1]);
  root.style.setProperty("--mdc-theme-text-disabled-on-background", colors.dark[1]);
  root.style.setProperty("--mdc-theme-text-disabled-on-dark", colors.light[1]);
  root.style.setProperty("--mdc-theme-text-disabled-on-light", colors.dark[1]);
};

export const saveTheme = (theme: Theme): void => {
  window.localStorage.setItem("theme", theme);
};

export const getTheme = (): Theme => {
  return (window.localStorage.getItem("theme") as Theme) || "dark";
};
