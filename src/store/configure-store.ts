import { applyMiddleware, createStore } from "redux";
import { routerMiddleware as createRouterMiddleware } from "connected-react-router";
import { composeWithDevTools } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";

// Local
import rootReducer from "./root-reducer";
import rootSaga from "./root-saga";

// Types
import type { Middleware, Store } from "redux";
import type { Action } from "redux-toolbelt";
import type { History } from "history";
import type { EnhancerOptions } from "redux-devtools-extension";
import type { RootState } from "./state";

interface ConfigureStore {
  (history: History): Store<RootState, Action>;
}

/**
 * Configre my Redux store
 *
 * @param history browser history for the router
 */
const configureStore: ConfigureStore = (history) => {
  // Configure middlewares
  const sagaMiddleware = createSagaMiddleware();
  const routerMiddleware = createRouterMiddleware(history);
  const middlewares: Middleware[] = [sagaMiddleware, routerMiddleware];

  // All midlewares as enhancer
  const middlewareEnhancer = applyMiddleware(...middlewares);

  // All enhancers
  const enhancers = [middlewareEnhancer];

  // Compose with devtools
  const compose = composeWithDevTools({ trace: true, traceLimit: 25 } as EnhancerOptions);

  // Create store
  const store = createStore(rootReducer(history), compose(...enhancers));

  // Hot reload reducer
  if (process.env.NODE_ENV !== "production" && module.hot) {
    module.hot.accept("./root-reducer", () => store.replaceReducer(rootReducer(history)));
  }

  // Run Saga middleware
  sagaMiddleware.run(rootSaga);

  return store;
};

export default configureStore;
