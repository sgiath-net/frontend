import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import { firebaseReducer } from "react-redux-firebase";
import { firestoreReducer } from "redux-firestore";

// Local
import { navigationReducer } from "../navigation";
import { themeReducer } from "../theme";

// Types
import type { History } from "history";
import type { Reducer } from "redux";
import type { RootState } from "./state";

const rootReducer = (history: History) =>
  combineReducers<RootState>({
    // Drawer state
    drawer: navigationReducer,
    // Theme state
    theme: themeReducer,
    // React Redux Firebase
    firebase: firebaseReducer,
    // Redux Firestore
    firestore: firestoreReducer as Reducer,
    // Connected React Router
    router: connectRouter(history),
  });

export default rootReducer;
