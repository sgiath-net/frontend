export { default as configureStore } from "./configure-store";

export type { RootState, SgiathStore } from "./state";
