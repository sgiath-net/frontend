import { all } from "redux-saga/effects";

// Local
import { analyticsSaga } from "../analytics";
import { themeSaga } from "../theme";

export default function* rootSaga() {
  yield all([analyticsSaga(), themeSaga()]);
}
