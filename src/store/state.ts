import type { Store } from "redux";
import type { Action } from "redux-toolbelt";
import type { RouterState } from "connected-react-router";
import type { FirebaseReducer, FirestoreReducer } from "react-redux-firebase";

import type { Theme } from "../theme";
import type { NavigationState } from "../navigation";

// Properties from User profile used in the app
interface UserProfile {
  avatarUrl: string;
  displayName: string;
}

export interface RootState {
  readonly drawer: NavigationState;
  readonly theme: Theme;
  readonly firebase: FirebaseReducer.Reducer<UserProfile>;
  readonly firestore: FirestoreReducer.Reducer;
  readonly router: RouterState;
}

export interface SgiathStore extends Store<RootState, Action> {}
