/* eslint-disable */
importScripts("https://www.gstatic.com/firebasejs/7.19.1/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/7.19.1/firebase-messaging.js");

// Initialize the Firebase app in the service worker by passing in the messagingSenderId.
firebase.initializeApp({
  apiKey: "AIzaSyDuKfa-kFfcvMeohvjKLB58Qy16BlQnAcs",
  authDomain: "sgiath-net.firebaseapp.com",
  databaseURL: "https://sgiath-net.firebaseio.com",
  projectId: "sgiath-net",
  storageBucket: "sgiath-net.appspot.com",
  messagingSenderId: "987905182371",
  appId: "1:987905182371:web:d4fd9ba6f10a213ed10d16",
  measurementId: "G-PJPJ21SHLZ",
});

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
  console.log("[firebase-messaging-sw.js] Received background message ", payload);

  // Customize notification here
  const notificationTitle = "Background Message Title";
  const notificationOptions = {
    body: "Background Message body.",
    icon: "/apple-touch-icon.png",
  };

  return self.registration.showNotification(notificationTitle, notificationOptions);
});
