module.exports = {
  root: true,
  reportUnusedDisableDirectives: true,
  extends: "react-app",
  env: {
    browser: true,
    serviceworker: true,
  },
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: "module",
  },
};
